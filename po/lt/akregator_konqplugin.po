# translation of akregator_konqplugin.po to Lithuanian
#
# Donatas Glodenis <dgvirtual@akl.lt>, 2005.
# Tomas Straupis <tomasstraupis@gmail.com>, 2009-2010, 2011.
# Remigijus Jarmalavičius <remigijus@jarmalavicius.lt>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: akregator_konqplugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-10-01 00:18+0000\n"
"PO-Revision-Date: 2019-07-04 22:36+0300\n"
"Last-Translator: Moo\n"
"Language-Team: Lithuanian <kde-i18n-lt@kde.org>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"
"X-Generator: Poedit 2.2.1\n"

#: akregatorplugin.cpp:63
#, kde-format
msgctxt "@action:inmenu"
msgid "Add Feed to Akregator"
msgstr "Pridėti kanalą į Akregator"

#: konqfeedicon.cpp:127
#, kde-format
msgid "Add Feed to Akregator"
msgstr "Pridėti kanalą į Akregator"

#: konqfeedicon.cpp:129
#, kde-format
msgid "Add Feeds to Akregator"
msgstr "Pridėti kanalus į Akregator"

#: konqfeedicon.cpp:138
#, kde-format
msgid "Add All Found Feeds to Akregator"
msgstr "Pridėti visus aptiktus kanalus į Akregator"

#: konqfeedicon.cpp:162
#, kde-format
msgid "Subscribe to site updates (using news feed)"
msgstr "Prenumeruoti šios svetainės atnaujinimus (naudojant naujienų kanalą)"

#: pluginutil.cpp:36 pluginutil.cpp:46
#, kde-format
msgid "Imported Feeds"
msgstr "Importuoti kanalai"

#: pluginutil.cpp:38
#, fuzzy, kde-format
#| msgid "Unable to contact Akregator via DBus"
msgid "Unable to contact Akregator via D-Bus"
msgstr "Nepavyko susisiekti su Akregator per DBus"

#: pluginutil.cpp:39
#, fuzzy, kde-format
#| msgctxt "@title:window"
#| msgid "DBus call failed"
msgctxt "@title:window"
msgid "D-Bus call failed"
msgstr "DBus iškvieta patyrė nesėkmę"

#~ msgid "Akregator feed icon - DBus Call failed"
#~ msgstr "Akregatoriaus piktograma: DBus kvietimas nepavyko"
