# translation of fsview.po to Khmer
# translation of fsview.po to
# Khoem Sokhem <khoemsokhem@khmeros.info>, 2006, 2007, 2008, 2010.
# Auk Piseth <piseth_dv@khmeros.info>, 2007, 2008.
msgid ""
msgstr ""
"Project-Id-Version: fsview\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-07-14 00:45+0000\n"
"PO-Revision-Date: 2010-06-09 08:36+0700\n"
"Last-Translator: Khoem Sokhem <khoemsokhem@khmeros.info>\n"
"Language-Team: Khmer <support@khmeros.info>\n"
"Language: km\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: KBabel 1.11.4\n"
"X-Language: km_KH\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "ខឹម សុខែម, ​អេង វណ្ណៈ, អោក ពិសិដ្ឋ​"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "khoemsokhem@khmeros.info,​​evannak@khmeros.info,piseth_dv@khmeros.info"

#: fsview.cpp:35 fsview.cpp:413
#, kde-format
msgid "Name"
msgstr "ឈ្មោះ"

#: fsview.cpp:36
#, kde-format
msgid "Size"
msgstr "ទំហំ"

#: fsview.cpp:37
#, kde-format
msgid "File Count"
msgstr "ចំនួន​ឯកសារ"

#: fsview.cpp:38
#, kde-format
msgid "Directory Count"
msgstr "ចំនួន​ថត"

#: fsview.cpp:39
#, kde-format
msgid "Last Modified"
msgstr "បាន​កែប្រែ​ចុង​ក្រោយ"

#: fsview.cpp:40 fsview.cpp:414
#, kde-format
msgid "Owner"
msgstr "ម្ចាស់"

#: fsview.cpp:41 fsview.cpp:415
#, kde-format
msgid "Group"
msgstr "ក្រុម"

#: fsview.cpp:42 fsview.cpp:416
#, kde-format
msgid "Mime Type"
msgstr "ប្រភេទ Mime"

#: fsview.cpp:283
#, kde-format
msgid "Go To"
msgstr "ទៅកាន់​​"

#: fsview.cpp:284 fsview_part.cpp:106
#, kde-format
msgid "Stop at Depth"
msgstr "បញ្ឈប់​នៅ​ជម្រៅ"

#: fsview.cpp:285 fsview_part.cpp:103
#, kde-format
msgid "Stop at Area"
msgstr "បញ្ឈប់​នៅ​ផ្ទៃ"

#: fsview.cpp:286
#, kde-format
msgid "Stop at Name"
msgstr "បញ្ឈប់​​ត្រង់​​ឈ្មោះ"

#: fsview.cpp:292
#, kde-format
msgid "Go Up"
msgstr "ទៅលើ"

#: fsview.cpp:294
#, kde-format
msgid "Stop Refresh"
msgstr "បញ្ឈប់​ការ​ធ្វើ​ឲ្យ​ស្រស់"

#: fsview.cpp:296
#, kde-format
msgid "Refresh"
msgstr "ធ្វើ​ឲ្យ​ស្រស់"

#: fsview.cpp:301
#, kde-format
msgid "Refresh '%1'"
msgstr "ធ្វើ '%1' ឲ្យ​ស្រស់"

#: fsview.cpp:313 fsview_part.cpp:113
#, kde-format
msgid "Color Mode"
msgstr "របៀប​ពណ៌"

#: fsview.cpp:316 fsview_part.cpp:109
#, kde-format
msgid "Visualization"
msgstr "រូបភាព​មើល​ឃើញ"

#: fsview.cpp:411
#, kde-format
msgid "None"
msgstr "គ្មាន"

#: fsview.cpp:412
#, kde-format
msgid "Depth"
msgstr "ជម្រៅ"

#: fsview_part.cpp:69
#, kde-format
msgid "Read 1 folder, in %2"
msgid_plural "Read %1 folders, in %2"
msgstr[0] "អានថត %1 នៅ​ក្នុង %2"

#: fsview_part.cpp:73
#, kde-format
msgid "1 folder"
msgid_plural "%1 folders"
msgstr[0] "ថត %1"

#: fsview_part.cpp:87
#, kde-format
msgid ""
"<p>This is the FSView plugin, a graphical browsing mode showing filesystem "
"utilization by using a tree map visualization.</p><p>Note that in this mode, "
"automatic updating when filesystem changes are made is intentionally <b>not</"
"b> done.</p><p>For details on usage and options available, see the online "
"help under menu 'Help/FSView Manual'.</p>"
msgstr ""
"<p>នេះគឺ​ជា​​កម្មវិធី​ជំនួយ FSView ​ការ​​រុករកដែល​មាន​​ក្រហ្វិក បង្ហាញ​ការ​ប្រើ​​ប្រាស់​​ប្រព័ន្ធ​ឯកសារ ដោយ​​​ប្រើ​"
"រូបភាព​​មើលឃើញ​ផែន​ទី​ជា​មែក​ធាង ។</p><p>ចំណាំថា នៅ​ក្នុង​របៀប​នេះ ការ​ធ្វើ​បច្ចុប្បន្នភាព​​ដោយ​ស្វ័យ​"
"ប្រវត្តិ នៅ​ពេល​​​ប្រព័ន្ធ​ឯកសារ​ត្រូវបាន​ផ្លាស់ប្ដូរ​ដោយ​ចេតនា <b>មិនត្រូវ​</b>​ បាន​ធ្វើ ។</p><p>សម្រាប់​​"
"សេចក្តី​លម្អិត​អំពី​​​ការប្រើ​ និង​ជម្រើស​ដែល​មាន​ ​សូម​មើល​ជំនួយ​លើ​ប្រព័ន្ធ​នៅ​​​​ម៉ឺនុយ ជំនួយ/សៀវភៅដៃ​របស់ FSView ។"
"</p>"

#: fsview_part.cpp:119
#, kde-format
msgid "&FSView Manual"
msgstr "សៀវភៅ​ដៃ​របស់ FSView"

#: fsview_part.cpp:121
#, kde-format
msgid "Show FSView manual"
msgstr "បង្ហាញ​សៀវភៅ​ដៃ​របស់ FSView"

#: fsview_part.cpp:122
#, kde-format
msgid "Opens the help browser with the FSView documentation"
msgstr "បើក​កម្មវិធី​រុករក​ជំនួយ​ជា​មួយ​នឹង​​ឯកសារ FSView"

#: fsview_part.cpp:154
#, kde-format
msgctxt "@action:inmenu File"
msgid "Move to Trash"
msgstr "ផ្លាស់ទីទៅធុង​សំរាម"

#: fsview_part.cpp:161
#, kde-format
msgctxt "@action:inmenu File"
msgid "Delete"
msgstr "លុប"

#: fsview_part.cpp:166
#, kde-format
msgctxt "@action:inmenu Edit"
msgid "&Edit File Type..."
msgstr "កែសម្រួល​ប្រភេទ​ឯកសារ..."

#: fsview_part.cpp:170
#, kde-format
msgctxt "@action:inmenu File"
msgid "Properties"
msgstr "លក្ខណសម្បត្តិ"

#: fsview_part.cpp:203
#, kde-format
msgid ""
"FSView intentionally does not support automatic updates when changes are "
"made to files or directories, currently visible in FSView, from the "
"outside.\n"
"For details, see the 'Help/FSView Manual'."
msgstr ""
"FSView មិន​គាំទ្រ​ការ​ធ្វើ​បច្ចុប្បន្នភាព​​ដោយស្វ័យប្រវត្តិទេ​នៅ​ពេល​ឯកសារ​ ឬ​​ថត​ត្រូវ​បាន​ផ្លាស់ប្ដូរ បច្ចុប្បន្ន​"
"មើល​ឃើញ​នៅ​ក្នុង FSView ពី​ខាង​ក្រៅ ។\n"
"សម្រាប់​សេចក្ដី​ល្អិត សូម​មើល ជំនួយ/សៀវភៅដៃ​របស់ FSView​ ។"

#. i18n: ectx: Menu (edit)
#: fsview_part.rc:4
#, kde-format
msgid "&Edit"
msgstr "កែសម្រួល"

#. i18n: ectx: Menu (view)
#: fsview_part.rc:13
#, kde-format
msgid "&View"
msgstr "មើល"

#. i18n: ectx: Menu (help)
#: fsview_part.rc:20
#, kde-format
msgid "&Help"
msgstr "ជំនួយ"

#: main.cpp:23
#, kde-format
msgid "FSView"
msgstr "​FSView"

#: main.cpp:24
#, kde-format
msgid "Filesystem Viewer"
msgstr "កម្មវិធី​មើល​ប្រព័ន្ធ​ឯកសារ"

#: main.cpp:26
#, kde-format
msgid "(c) 2002, Josef Weidendorfer"
msgstr "​​រក្សាសិទ្ធិ​ឆ្នាំ ២០០២ ដោយ Josef Weidendorfer"

#: main.cpp:33
#, kde-format
msgid "View filesystem starting from this folder"
msgstr "មើល​ការ​ចាប់ផ្តើម​ប្រព័ន្ធ​ឯកសារ​ពី​ថត​នេះ"

#: treemap.cpp:1436
#, kde-format
msgid "Text %1"
msgstr "អត្ថបទ %1"

#: treemap.cpp:3163
#, kde-format
msgid "Recursive Bisection"
msgstr "ការ​ចែក​ជា​ពី​ភាគ​ស្មើគ្នា​ដោយ​ខ្លួនឯង"

#: treemap.cpp:3165
#, kde-format
msgid "Columns"
msgstr "ជួរឈរ"

#: treemap.cpp:3167
#, kde-format
msgid "Rows"
msgstr "ជួរដេក"

#: treemap.cpp:3169
#, kde-format
msgid "Always Best"
msgstr "ល្អបំផុត​ជា​និច្ច"

#: treemap.cpp:3171
#, kde-format
msgid "Best"
msgstr "ល្អបំផុត"

#: treemap.cpp:3173
#, kde-format
msgid "Alternate (V)"
msgstr "ជំនួស (V)"

#: treemap.cpp:3175
#, kde-format
msgid "Alternate (H)"
msgstr "ជំនួស (H)"

#: treemap.cpp:3177
#, kde-format
msgid "Horizontal"
msgstr "ផ្ដេក"

#: treemap.cpp:3179
#, kde-format
msgid "Vertical"
msgstr "បញ្ឈរ"

#: treemap.cpp:3231
#, kde-format
msgid "Nesting"
msgstr "ក្នុង​គ្នា"

#: treemap.cpp:3235
#, kde-format
msgid "Border"
msgstr "ស៊ុម"

#: treemap.cpp:3238
#, kde-format
msgid "Correct Borders Only"
msgstr "កែតែ​ស៊ុម"

#: treemap.cpp:3241
#, kde-format
msgid "Width %1"
msgstr "ទទឹង %1"

#: treemap.cpp:3244
#, kde-format
msgid "Allow Rotation"
msgstr "អនុញ្ញាត​ឲ្យ​បង្វិល"

#: treemap.cpp:3245
#, kde-format
msgid "Shading"
msgstr "ការ​​ដាក់​ស្រមោល"

#: treemap.cpp:3256
#, kde-format
msgid "Visible"
msgstr "អាច​មើល​ឃើញ"

#: treemap.cpp:3257
#, kde-format
msgid "Take Space From Children"
msgstr "យក​​ចន្លោះ​ពី​កូនចៅ"

#: treemap.cpp:3261
#, kde-format
msgid "Top Left"
msgstr "កំពូល​​​ឆ្វេង"

#: treemap.cpp:3264
#, kde-format
msgid "Top Center"
msgstr "កំពូល​កណ្ដាល"

#: treemap.cpp:3267
#, kde-format
msgid "Top Right"
msgstr "កំពូល​ស្ដាំ"

#: treemap.cpp:3270
#, kde-format
msgid "Bottom Left"
msgstr "បាត​ឆ្វេង"

#: treemap.cpp:3273
#, kde-format
msgid "Bottom Center"
msgstr "បាត​កណ្ដាល"

#: treemap.cpp:3276
#, kde-format
msgid "Bottom Right"
msgstr "បាត​​ស្ដាំ"

#: treemap.cpp:3343
#, kde-format
msgid "No %1 Limit"
msgstr "គ្មាន​ព្រំដែន %1"

#: treemap.cpp:3402
#, kde-format
msgid "No Area Limit"
msgstr "គ្មាន​ព្រំដែន​ផ្ទៃ"

#: treemap.cpp:3408
#, kde-format
msgid "Area of '%1' (%2)"
msgstr "ផ្ទៃ​របស់ '%1' (%2)"

#: treemap.cpp:3420 treemap.cpp:3433
#, kde-format
msgid "1 Pixel"
msgid_plural "%1 Pixels"
msgstr[0] "%1 ភីកសែល"

#: treemap.cpp:3437
#, kde-format
msgid "Double Area Limit (to %1)"
msgstr "ព្រំដែន​ផ្ទៃ​ទ្វេ​​ (ទៅ %1)"

#: treemap.cpp:3439
#, kde-format
msgid "Halve Area Limit (to %1)"
msgstr "ព្រំដែន​ផ្ទៃ​ចែក​ជាពីរ (ទៅ %1)"

#: treemap.cpp:3475
#, kde-format
msgid "No Depth Limit"
msgstr "គ្មាន​ព្រំដែន​ជម្រៅ"

#: treemap.cpp:3481
#, kde-format
msgid "Depth of '%1' (%2)"
msgstr "ជម្រៅ​របស់ '%1' (%2)"

#: treemap.cpp:3493 treemap.cpp:3506
#, kde-format
msgid "Depth %1"
msgstr "ជម្រៅ %1"

#: treemap.cpp:3510
#, kde-format
msgid "Decrement (to %1)"
msgstr "បន្ថយ (ទៅ %1)"

#: treemap.cpp:3512
#, kde-format
msgid "Increment (to %1)"
msgstr "​បង្កើន (ទៅ %1)"

#~ msgid "Filesystem Utilization Viewer"
#~ msgstr "កម្មវិធី​មើល​ការ​ប្រើប្រាស់​ប្រព័ន្ធ​​ឯកសារ"

#~ msgid "(c) 2003-2005, Josef Weidendorfer"
#~ msgstr "រក្សាសិទ្ធិ​ឆ្នាំ ២០០៣-២០០៥ ដោយ Josef Weidendorfer"
