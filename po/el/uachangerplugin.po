# translation of uachangerplugin.po to Greek
# Copyright (C) 2003, 2005, 2007 Free Software Foundation, Inc.
#
# Stergios Dramis <sdramis@egnatia.ee.auth.gr>, 2002-2003.
# Spiros Georgaras <sng@hellug.gr>, 2005, 2007.
# Glentadakis Dimitrios <dglent@gmail.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: uachangerplugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-07-14 00:45+0000\n"
"PO-Revision-Date: 2009-06-14 10:04+0200\n"
"Last-Translator: Glentadakis Dimitrios <dglent@gmail.com>\n"
"Language-Team: Greek <kde-i18n-doc@kde.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 0.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: uachangerplugin.cpp:43
#, kde-format
msgid "Change Browser Identification"
msgstr "Αλλαγή ταυτότητας περιηγητή"

#: uachangerplugin.cpp:108
#, kde-format
msgid "Other"
msgstr "Άλλο"

#: uachangerplugin.cpp:136
#, kde-format
msgctxt "%1 = browser version (e.g. 2.0)"
msgid "Version %1"
msgstr "Έκδοση %1"

#: uachangerplugin.cpp:139
#, kde-format
msgctxt "%1 = browser name, %2 = browser version (e.g. Firefox, 2.0)"
msgid "%1 %2"
msgstr "%1 %2"

#: uachangerplugin.cpp:143
#, kde-format
msgctxt "%1 = browser version, %2 = platform (e.g. 2.0, Windows XP)"
msgid "Version %1 on %2"
msgstr "Έκδοση %1 σε %2"

#: uachangerplugin.cpp:146
#, kde-format
msgctxt ""
"%1 = browser name, %2 = browser version, %3 = platform (e.g. Firefox, 2.0, "
"Windows XP)"
msgid "%1 %2 on %3"
msgstr "%1 %2 on %3"

#: uachangerplugin.cpp:199
#, kde-format
msgid "Default Identification"
msgstr "Προκαθορισμένη ταυτότητα"

#. i18n("Reload Identifications"), this,
#. SLOT(slotReloadDescriptions()),
#. 0, ++count );
#: uachangerplugin.cpp:227
#, kde-format
msgid "Apply to Entire Site"
msgstr "Εφαρμογή σε ολόκληρο το δικτυακό τόπο"

#: uachangerplugin.cpp:233
#, kde-format
msgid "Configure..."
msgstr "Ρύθμιση..."

#. i18n: ectx: Menu (tools)
#: uachangerplugin.rc:4
#, kde-format
msgid "&Tools"
msgstr "Ερ&γαλεία"

#. i18n: ectx: ToolBar (extraToolBar)
#: uachangerplugin.rc:8
#, kde-format
msgid "Extra Toolbar"
msgstr "Επιπρόσθετη γραμμή εργαλείων"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Σπύρος Γεωργαράς"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "sng@hellug.gr"

#~ msgid "Change Browser &Identification"
#~ msgstr "Αλλαγή τα&υτότητας περιηγητή"

#~ msgid "Identify As"
#~ msgstr "Ταυτοποίηση ως"
