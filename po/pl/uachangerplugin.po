# Version: $Revision: 1629187 $
# translation of uachangerplugin.po to
# Copyright (C) 2002, 2004, 2007 Free Software Foundation, Inc.
#
# Marcin Giedz <mgiedz@elka.pw.edu.pl>, 2002.
# Krzysztof Lichota <lichota@mimuw.edu.pl>, 2004.
# Marta Rybczyńska <kde-i18n@rybczynska.net>, 2007.
# Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>, 2011, 2019.
msgid ""
msgstr ""
"Project-Id-Version: uachangerplugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-07-14 00:45+0000\n"
"PO-Revision-Date: 2019-06-20 20:50+0200\n"
"Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>\n"
"Language-Team: Polish <kde-i18n-doc@kde.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 19.07.70\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#: uachangerplugin.cpp:43
#, kde-format
msgid "Change Browser Identification"
msgstr "Zmień identyfikację przeglądarki"

#: uachangerplugin.cpp:108
#, kde-format
msgid "Other"
msgstr "Inna"

#: uachangerplugin.cpp:136
#, kde-format
msgctxt "%1 = browser version (e.g. 2.0)"
msgid "Version %1"
msgstr "Wersja %1"

#: uachangerplugin.cpp:139
#, kde-format
msgctxt "%1 = browser name, %2 = browser version (e.g. Firefox, 2.0)"
msgid "%1 %2"
msgstr "%1 %2"

#: uachangerplugin.cpp:143
#, kde-format
msgctxt "%1 = browser version, %2 = platform (e.g. 2.0, Windows XP)"
msgid "Version %1 on %2"
msgstr "Wersja %1 na %2"

#: uachangerplugin.cpp:146
#, kde-format
msgctxt ""
"%1 = browser name, %2 = browser version, %3 = platform (e.g. Firefox, 2.0, "
"Windows XP)"
msgid "%1 %2 on %3"
msgstr "%1 %2 na %3"

#: uachangerplugin.cpp:199
#, kde-format
msgid "Default Identification"
msgstr "Domyślna identyfikacja"

#. i18n("Reload Identifications"), this,
#. SLOT(slotReloadDescriptions()),
#. 0, ++count );
#: uachangerplugin.cpp:227
#, kde-format
msgid "Apply to Entire Site"
msgstr "Zastosuj dla całej strony"

#: uachangerplugin.cpp:233
#, kde-format
msgid "Configure..."
msgstr "Ustawienia..."

#. i18n: ectx: Menu (tools)
#: uachangerplugin.rc:4
#, kde-format
msgid "&Tools"
msgstr "&Narzędzia"

#. i18n: ectx: ToolBar (extraToolBar)
#: uachangerplugin.rc:8
#, kde-format
msgid "Extra Toolbar"
msgstr "Dodatkowy pasek narzędzi"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Krzysztof Lichota"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "lichota@mimuw.edu.pl"

#~ msgid "Change Browser &Identification"
#~ msgstr "Zmień &identyfikację przeglądarki"

#~ msgid "Identify As"
#~ msgstr "Przedstawiaj się jako"
